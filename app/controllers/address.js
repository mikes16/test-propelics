var args = arguments[0] || {};

if(args.address.length > 0){
	$.street.value = args.address[0];
	$.city.value = args.address[1];
	$.state.value = args.address[2];
	$.zip.value = args.address[3];
}


function save () {
	args.callback({
		address: {
			street:$.street.value,
			city:$.city.value,
			state:$.state.value,
			zip:$.zip.value
		}
	});
	Alloy.Globals.contact_tab.close($.window);
	if (OS_ANDROID) {
		$.window.close();
	}
	
}
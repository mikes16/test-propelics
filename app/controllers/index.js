Alloy.Globals.tbgrp=$.tbgrp ; 
Alloy.Globals.contact_tab = $.tab1;
Alloy.Globals.tab1=$.win1;
Alloy.Globals.tab2=$.win3;
Alloy.Globals.group_tab=$.tab3;

//Get the contacts from the Database to fill the table
var contacts = Alloy.Globals.db.getContacts();
var groups = Alloy.Globals.db.getGroups();


/*===========================
 	EVENT LISTENERS
 =============================*/
Ti.App.addEventListener('refreshContacts', function (){
	$.cntctlist.data = [];
	contacts = Alloy.Globals.db.getContacts();
	for (var i=0;i<contacts.length;i++) {
		addContact(contacts[i], contacts[i].isfav);
	};
});

$.gridviewbtn.addEventListener("click", function (e) {
	contacts = Alloy.Globals.db.getContacts();
	Alloy.Globals.list = $.win1view;
	$.win1.remove($.win1view);
	var gridwindow = Alloy.createController('_grid_view', {
		contacts : contacts
	});
	Alloy.Globals.grid = gridwindow;
	$.win1.add(gridwindow.getView());
});

$.cntctlist.addEventListener('delete', function(evt){
	deleteUser(evt.rowData.id);
});

//SORT'S
$.sortView.addEventListener('click', function(evt){

	switch(evt.source.id){
		case 'favoritbtnn':
			$.cntctlist.data = [];
			
			$.sortName.image = "/images/sortNameNS.png";
			$.sortLast.image = "/images/sortLastNS.png";
			$.sortAge.image = "/images/sortAgeNS.png";
			// $.sortPhone.image = "/images/sortPhoneNS.png";
			
			if($.favoritbtnn.image === "/images/fav.png"){
				$.favoritbtnn.image = '/images/favSel.png';
				contacts = Alloy.Globals.db.getContacts();
				for (var i=0;i<contacts.length;i++) {
					if(contacts[i].isfav > 0){
						addContact(contacts[i], contacts[i].isfav);			
					}
				};
				
			}else{
				$.favoritbtnn.image = '/images/fav.png';
				for (var i=0;i<contacts.length;i++) {
					addContact(contacts[i], contacts[i].isfav);
				};
			}
			
			break;
		case 'sortName':
			$.cntctlist.data = [];
			
			$.favoritbtnn.image = "/images/fav.png";
			$.sortLast.image = "/images/sortLastNS.png";
			$.sortAge.image = "/images/sortAgeNS.png";
			// $.sortPhone.image = "/images/sortPhoneNS.png";
		
			if($.sortName.image == "/images/sortName.png"){
				$.sortName.image = "/images/sortNameAsc.png";
				contacts = Alloy.Globals.db.getContactsOrdered('firstName', 'ASC');
				for (var i=0;i<contacts.length;i++) {
					if(contacts[i].isfav > 0){
						addContact(contacts[i], contacts[i].isfav);			
					}
				};
			}else{
				$.sortName.image = "/images/sortName.png";
				contacts = Alloy.Globals.db.getContactsOrdered('firstName', 'DESC');
				for (var i=0;i<contacts.length;i++) {
					if(contacts[i].isfav > 0){
						addContact(contacts[i], contacts[i].isfav);			
					}
				};
			}
			
			break;
		case 'sortLast': 
			$.cntctlist.data = [];
			
			$.favoritbtnn.image = "/images/fav.png";
			$.sortName.image = "/images/sortNameNS.png";
			$.sortAge.image = "/images/sortAgeNS.png";
			// $.sortPhone.image = "/images/sortPhoneNS.png";
		
			if($.sortLast.image == "/images/sortLast.png"){
				$.sortLast.image = "/images/sortLastAsc.png";
				
				contacts = Alloy.Globals.db.getContactsOrdered('lastName', 'ASC');
				for (var i=0;i<contacts.length;i++) {
					if(contacts[i].isfav > 0){
						addContact(contacts[i], contacts[i].isfav);			
					}
				};
			}else{
				$.sortLast.image = "/images/sortLast.png";
				
				contacts = Alloy.Globals.db.getContactsOrdered('lastName', 'DESC');
				for (var i=0;i<contacts.length;i++) {
					if(contacts[i].isfav > 0){
						addContact(contacts[i], contacts[i].isfav);			
					}
				};
			}
			
			break;
		case 'sortAge':
			$.cntctlist.data = [];
			
			$.favoritbtnn.image = "/images/fav.png";
			$.sortName.image = "/images/sortNameNS.png";
			$.sortLast.image = "/images/sortLastNS.png";
			// $.sortPhone.image = "/images/sortPhoneNS.png";
		
			if($.sortAge.image == "/images/sortAge.png"){
				$.sortAge.image = "/images/sortAgeAsc.png";
				
				contacts = Alloy.Globals.db.getContactsOrdered('CAST(dob AS int)', 'ASC');
				for (var i=0;i<contacts.length;i++) {
					addContact(contacts[i], contacts[i].isfav);		
				};
			}else{
				$.sortAge.image = "/images/sortAge.png";
				
				contacts = Alloy.Globals.db.getContactsOrdered('CAST(dob AS int)', 'DESC');
				for (var i=0;i<contacts.length;i++) {
					addContact(contacts[i], contacts[i].isfav);			
				};
			}
			
			break;
		case 'sortPhone':
			
			// $.favoritbtnn.image = "/images/fav.png";
			// $.sortName.image = "/images/sortNameNS.png";
			// $.sortLast.image = "/images/sortLastNS.png";
			// $.sortAge.image = "/images/sortAgeNS.png";
// 		
			// if($.sortPhone.image == "/images/sortPhone.png"){
				// $.sortPhone.image = "/images/sortPhoneAsc.png";
			// }else{
				// $.sortPhone.image = "/images/sortPhone.png";
			// }
		
			break;
		default: break;
	}
});

/*===========================
 	END EVENT LISTENERS
===========================*/

// FILL THE TABLE WITH THE CONTACTS WE WHET BEFORE
for (var i=0;i<contacts.length;i++) {
	addContact(contacts[i], contacts[i].isfav);
};

//*************************//
//********FUNCTIONS********//
//*************************//

function addContact(contact, isfav, index){
	var meObj, doNothing = false;
	
	if(Ti.App.Properties.hasProperty('me')){
		meObj = Ti.App.Properties.getObject('me').contact;
		changeMyData(meObj);
		if(contact.firstName === meObj.firstName && contact.phone === meObj.phone && contact.lastName === meObj.lastName){
			doNothing = true;
		}
	}
	
	if(!doNothing){
		var cntctrow=Ti.UI.createTableViewRow({
			height:Ti.UI.SIZE,
			width:Ti.UI.SIZE,
			backgroundSelectedColor: 'transparent',
			contact: contact,
			isfav: isfav,
			id: contact.id
		});
		
		if(OS_IOS){
			cntctrow.selectedBackgroundColor = 'transparent';
		}
		
		var row =Alloy.createController('cntct_Row', 	
		{
			image: contact.photo,
			id: contact.id,
			firstName: contact.firstName,
			lastName: contact.lastName,
			phone: contact.phone,
			isfav: isfav,
			type: 1
		});
		
		cntctrow.add(row.getView());	
				
		// Function to make the user clicked as 'me' on the top
		cntctrow.addEventListener('click', function(evt){
			var me = {
				contact: evt.rowData.contact,
				isfav: evt.rowData.isfav,
				index: evt.index
			};
			switch(evt.source.id){
				case 'favIcon':
					evt.source.image = (evt.source.image === '/images/favSel.png') ? '/images/fav.png': '/images/favSel.png'; 
					var isFav;
					if(evt.source.image === '/images/favSel.png'){
						isFav = 1;
						evt.rowData.isfav = 1;
					}else{
						isFav = 0;	
						evt.rowData.isfav = 0;
					};
					
					Alloy.Globals.db.contactFav(isFav, evt.rowData.contact.id);
					break;
				case 'edit' : 
					editContact(evt.rowData.contact);
					break;
				case 'pin' : 
					$.cntctlist.deleteRow(evt.row);
					var oldMe;
					
					//Verify if already have an object pinged, if not, this will be the first.
					if(Ti.App.Properties.hasProperty('me')){
						oldMe = Ti.App.Properties.getObject('me');						
					}else{
						oldMe = me;
					}
					
					Ti.App.Properties.setObject('me', me);
					addContact(oldMe.contact,  oldMe.isfav, oldMe.index);
					break;
				case 'remove':
					$.cntctlist.deleteRow(evt.row);
					deleteUser(evt.rowData.contact.id);
					break;
				default : break;
			}
		});
		
		//I Send an index only when a row is clicked otherwhise just append a Row in the bottom.
		if(index != null){
			$.cntctlist.insertRowBefore(index, cntctrow);
		}else{
			$.cntctlist.appendRow(cntctrow);			
		}
		
	}
}

// FUNCTION TO REMOVE A USER FROM THE TABLE AND DELETE IT FROM DB
function deleteUser(id){
	Alloy.Globals.db.deleteContact(id);
}

// OPEN A NEW WINDOW TO CREATE A CONTACT
function createContact (e) {
	console.log("createContact()");
	var newContact = Alloy.createController('newContact', {

	});
	$.tab1.open(newContact.window);
}

// OPEN A NEW WINDOW TO EDIT A CONTACT
function editContact(contact) {
	console.log("editContact()");
	var editContact = Alloy.createController('newContact', {
		contact: contact
	});
	
	$.tab1.open(editContact.window);
}

//Function to fill the 'ME' data at the top of the page
function changeMyData(me){
	if(!$.me.getVisible()){
		$.me.visible = true;
		$.me.height = 100;
	}
	
	$.myphoto.image = (me.photo) ? me.photo.thumbnail: '';
	$.myname.text = me.firstName + ' ' + me.lastName;
	$.mynumnber.text = me.phone;
}


//********VALIDATIONS********//

if (OS_ANDROID) {
	$.tbgrp.addEventListener('open', function(evt){
	    $.tbgrp.activity.invalidateOptionsMenu();
	});
}

$.tbgrp.open();


/******************************
 * 
 * 
 * !!!!!!!!  GROUPS !!!!!!!!!!
 * 
 * 
 ******************************/
 
function createGroup(){
 	var newGroup = Alloy.createController('newGroup', {
	
	});
	$.tab3.open(newGroup.window);
 }
 
 /*DELETE GROUP*/
 $.grplist.addEventListener('delete', function(evt){
	deleteGroup(evt.rowData.id);
});

// FUNCTION TO REMOVE A USER FROM THE TABLE AND DELETE IT FROM DB
function deleteGroup(id){
	Alloy.Globals.db.deleteGroup(id);
}
 
// FILL THE TABLE WITH THE GROUPS DB
for (var i=0;i<groups.length;i++) {
	addGroup(groups[i], groups[i].isfav);
};

// OPEN A NEW WINDOW TO EDIT A CONTACT
function editGroup(group) {
	var editGroup_win = Alloy.createController('newGroup', {
		group: group
	});
	
	$.tab3.open(editGroup_win.window);
}

function addGroup(group, isfav){
	
	var groupRow = Ti.UI.createTableViewRow({
		height:Ti.UI.SIZE,
		width:Ti.UI.SIZE,
		backgroundSelectedColor: 'transparent',
		group: group,
		isfav: isfav,
		id: group.id
	});
	
	if(OS_IOS){
		groupRow.selectedBackgroundColor = 'transparent';
	}
	
	var row =Alloy.createController('cntct_Row', 	
	{
		image: group.photo,
		id: group.id,
		firstName: group.title,
		phone: group.description,
		isfav: isfav,
		type: 2
	});
	
	groupRow.add(row.getView());	
			
	groupRow.addEventListener('click', function(evt){
		switch(evt.source.id){
			case 'favIcon':
				evt.source.image = (evt.source.image === '/images/favSel.png') ? '/images/fav.png': '/images/favSel.png'; 
				var isFav;
				if(evt.source.image === '/images/favSel.png'){
					isFav = 1;
					evt.rowData.isfav = 1;
				}else{
					isFav = 0;	
					evt.rowData.isfav = 0;
				};
				
				Alloy.Globals.db.groupFav(isFav, evt.rowData.id);
				break;
			case 'edit' : 
				editGroup(evt.rowData.group);
				break;
			case 'remove':
				$.grplist.deleteRow(evt.row);
				deleteGroup(evt.rowData.group.id);
				break;
			default : break;
		}
	});
	
	$.grplist.appendRow(groupRow);			
		
}

Ti.App.addEventListener('refreshGroups', function (){
	var groups = Alloy.Globals.db.getGroups();
	$.grplist.data = [];
	
	for (var i=0;i<groups.length;i++) {
		addGroup(groups[i], groups[i].isfav);
	};
});

$.favoritbtn_grp.addEventListener("click", function (e) {
	$.grplist.data = [];
	
	if($.favoritbtn_grp.image === "/images/fav.png"){
		$.favoritbtn_grp.image = '/images/favSel.png';
		var groups = Alloy.Globals.db.getGroupFavs();
		
		for (var i=0;i<groups.length;i++) {
			addGroup(groups[i], groups[i].isfav);
		};
		
	}else{
		var groups = Alloy.Globals.db.getGroups();
		
		$.favoritbtn_grp.image = '/images/fav.png';
		for (var i=0;i<groups.length;i++) {
			addGroup(groups[i], groups[i].isfav);
		};
	}
});

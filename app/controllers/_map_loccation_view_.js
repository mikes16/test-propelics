var args = arguments[0] || {};

var lat = (args.position.latitude) ? args.position.latitude : 21.432490, 
	lon = (args.position.longitude) ? args.position.longitude : -100.782158;

var mountainView = Alloy.Globals.Map.createAnnotation({
    latitude : lat,
    longitude : lon,
    pincolor : Alloy.Globals.Map.ANNOTATION_RED,
    draggable: true,
    title: (args.firstName) ? args.firstName : 'UNNAMED', 
});
$.mapview.addAnnotation(mountainView);

$.mapview.region = {
	latitude : lat,
    longitude : lon,
    latitudeDelta : 0.01,
    longitudeDelta : 0.01
};

// Handle click events on any annotations on this map.
function getLocations(evt){
	lat = evt.annotation.latitude;
	lon = evt.annotation.longitude;
}

function save(){
	args.callback({
		position: {
			latitude: lat,
			longitude: lon
		}
	});
	$.window.close();
}

var args = arguments[0] || {};
var dateofbirth,picker,lat,lon, large, medium, thumbnail, date, address_json, isUpdate;
var monthNames = [
	"January", "February", "March", "April", "May", "June",
	"July", "August", "September", "October", "November", "December"
];
console.log(args.contact);

if(args.contact){
	isUpdate = true;
	/*INIT*/
	lat = args.contact.latitude;
	lon = args.contact.longitude;
	$.location.value = lat + ', ' + lon;
	
	date = new Date(parseInt(args.contact.dob));
	$.dateofbirth.value = (date.getFullYear() + ' - ' + monthNames[date.getMonth()] + ' - ' + (date.getDate()));
	
	var addrs = [];
	address_json = args.contact.address;
	for (var address  in args.contact.address){
		if (args.contact.address[address] !== '' && args.contact.address[address] != null && args.contact.address[address] != undefined)
			addrs.push(args.contact.address[address]);
	}
	$.addresstext.value = addrs.join(", ");
	$.first.value = args.contact.firstName;
	$.last.value = args.contact.lastName;
	$.gender.value = args.contact.gender;
	$.company.value = args.contact.company;
	$.role.value = args.contact.role;
	$.email.value = args.contact.email;
	$.phone.value = args.contact.phone;
	$.notes.value = args.contact.notes;
	$.photoimg.image=args.contact.photo.thumbnail;
}

function save () {
	if(isUpdate){
		if($.first.value != '' && $.last.value != ''){
			Alloy.Globals.db.updateContact({
				id : args.contact.id,
				isfav : 0,
				firstName : $.first.value,
				lastName : $.last.value,
				gender : $.gender.value,
				company : $.company.value,
				role : $.role.value,
				email : $.email.value,
				phone : $.phone.value,
				notes : $.notes.value,
				dob : (date) ? date.getTime() : '',
				photo : args.contact.photo,
				latitude: (lat) ? lat : '',
				longitude: (lon) ? lon : '',
				address: (address_json) ? address_json : ''
			});
			
			Ti.App.fireEvent('refreshContacts');
			Alloy.Globals.contact_tab.close($.window);
			
			if (OS_ANDROID) {
				$.window.close();
			}
			
		}else{
			alert('You need to set a name and last name to continue');
		}
	}else{
		if($.first.value != '' && $.last.value != ''){
			
			if(large && medium && thumbnail){
				var largeImg = Ti.Filesystem.getFile(Ti.Filesystem.applicationDataDirectory,'large_'+$.first.value+$.last.value+'.jpg');
				largeImg.write(large);
				
				var mediumImg = Ti.Filesystem.getFile(Ti.Filesystem.applicationDataDirectory,'medium_'+$.first.value+$.last.value+'.jpg');
				mediumImg.write(medium);
				
				var thumbnailImg = Ti.Filesystem.getFile(Ti.Filesystem.applicationDataDirectory,'thumbnail_'+$.first.value+$.last.value+'.jpg');
				thumbnailImg.write(thumbnail);	
			}
			
			Alloy.Globals.db.insertContact({
				id : Ti.Platform.createUUID(),
				isfav : 0,
				firstName : $.first.value,
				lastName : $.last.value,
				gender : $.gender.value,
				company : $.company.value,
				role : $.role.value,
				email : $.email.value,
				phone : $.phone.value,
				notes : $.notes.value,
				dob : (date) ? date.getTime() : '',
				photo : {
					large: (large) ? largeImg.nativePath : '',
					medium: (large) ? mediumImg.nativePath : '',
					thumbnail: (large) ? thumbnailImg.nativePath : ''
				},
				latitude: (lat) ? lat : '',
				longitude: (lon) ? lon : '',
				address: (address_json) ? address_json : ''
				
			});
			Ti.App.fireEvent('refreshContacts');
			Alloy.Globals.contact_tab.close($.window);
			
			if (OS_ANDROID) {
				$.window.close();
			}
		}else{
			alert('You need to set a name and last name to continue');
		}
	}
}

function openMap(){
	maplocatiomn = Alloy.createController('_map_loccation_view_', {
		firstName: $.first.value,
		position:{
			latitude: lat,
			longitude: lon
		},
		callback:function(evt){
			lat = evt.position.latitude;
			lon = evt.position.longitude;
			$.location.value = lat + ', ' + lon;
		}
	});
	Alloy.Globals.contact_tab.open(maplocatiomn.getView());
}

function openAddress(){
	address=Alloy.createController("address", {
		address: $.addresstext.value.split(", "),
		callback:function(e2){
			var res = [];
			address_json = e2.address;
			for (var address  in e2.address){
				if (e2.address[address] !== '' && e2.address[address] != null && e2.address[address] != undefined)
				res.push(e2.address[address]);
			}
			$.addresstext.value = res.join(", ");
		}
	});
	Alloy.Globals.contact_tab.open(address.window);
}

//OPEN THE OPTIONS TO OPEN THE CAMERA
function openPhotoOptions(){
	$.photoOptions.show();
}

//JUST ACCESS THIS SECTION FROM IOS
function openGender(){
	//Create a view as a background
	var pick_viw = Ti.UI.createView({
		height: Ti.UI.FILL,
		width: Ti.UI.FILL,
		backgroundColor: "#000",
		opacity: .5,
		top: 0,
		botom: 0,
		left: 0,
		right:0
	});
	
	picker=Ti.UI.createPicker({
		type : Ti.UI.PICKER_TYPE_PLAIN,
		selectionIndicator : true,
       	bottom:-251,
        useSpinner : true,
		width:Ti.UI.FILL,
		height:210
	});
	
	var slide_in =  Titanium.UI.createAnimation({bottom:0});
	var slide_out =  Titanium.UI.createAnimation({bottom:-251});
	
	var data = [];
	data[0]=Ti.UI.createPickerRow({title:'Male'});
	data[1]=Ti.UI.createPickerRow({title:'Female'});
	picker.add(data);
	
	// Set a default value
	$.gender.value = 'Male';
	
	picker.addEventListener('change', function(evt){
		$.gender.value = evt.selectedValue[0];
	});
	
	pick_viw.addEventListener('click', function(evt){
		picker.animate(slide_out, function(){
			$.window.remove(pick_viw);
			$.window.remove(picker);
		});
		
	});

	$.window.add(pick_viw);
	$.window.add(picker);
	
	picker.animate(slide_in);
}

function showPhoto (e) {
	if(e.index==0){
		Ti.Media.showCamera({
			success:function(e2){
				large = e2.media.imageAsResized(512, 512);
				medium = e2.media.imageAsResized(150, 150);
				thumbnail = e2.media.imageAsResized(80, 80);
				$.photoimg.image=e2.media;
			}
		});
	}else if(e.index==1){
		Ti.Media.openPhotoGallery({
			success:function(e2){
				large = e2.media.imageAsResized(512, 512);
				medium = e2.media.imageAsResized(150, 150);
				thumbnail = e2.media.imageAsResized(80, 80);
				$.photoimg.image=e2.media;
			}
		});
	}
}

function createpicker(){
	//Create a view as a background
	var pick_viw = Ti.UI.createView({
		height: Ti.UI.FILL,
		width: Ti.UI.FILL,
		backgroundColor: "#000",
		opacity: .5,
		top: 0,
		botom: 0,
		left: 0,
		right:0
	});
	
	picker=Ti.UI.createPicker({
		type:Titanium.UI.PICKER_TYPE_DATE,
		width:Ti.UI.FILL,
		height:210,
		bottom:-251,
		width:Ti.UI.FILL,
		value: (date) ? date : new Date()
	});
	
	if(OS_IOS){
		
		$.window.add(pick_viw);
		$.window.add(picker);
		
		var slide_in =  Titanium.UI.createAnimation({bottom:0});
		var slide_out =  Titanium.UI.createAnimation({bottom:-251});
		
		picker.animate(slide_in);
		
		picker.addEventListener('change', function(e){
			date = new Date(e.value);
			$.dateofbirth.value = (date.getFullYear() + ' - ' + monthNames[date.getMonth()] + ' - ' + (date.getDate()));
		});
		
		pick_viw.addEventListener('click', function(evt){
			picker.animate(slide_out, function(){
				$.window.remove(pick_viw);
				$.window.remove(picker);
			});
			
		});
		
	}else{
		picker.showDatePickerDialog({
			callback:function(e){
				if(e.cancel){

				}else{
					$.dateofbirth.value = (date.getFullYear() + ' - ' + monthNames[date.getMonth()] + ' - ' + (date.getDate()+1));
					date= new Date(e.value);
				}
			}
		});
	}
}

function removeImage(){
	var dialog = Ti.UI.createAlertDialog({
	    cancel: 1,
	    buttonNames: ['Confirm', 'Cancel'],
	    message: 'Would you like to remove the image?',
	    title: 'Remove'
	});
	 
	dialog.addEventListener('click', function(e){
	    if (e.index === e.source.cancel){
	      	Ti.API.info('The cancel button was clicked');
	    }else{
	    	$.photoimg.image = "";	
	    	large = "";
			medium = "";
			thumbnail = "";
	    }
	});
	dialog.show();
}

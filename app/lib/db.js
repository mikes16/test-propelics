var db = Ti.Database.install("addressBook.sql", "addressBook");

exports.insertContact = function(contact){
	db.execute("INSERT INTO contact
		(
			id,
			isfav,
			firstName,
			lastName,
			gender,
			company,
			role,
			email,
			phone,
			notes,
			latitude,
			longitude,
			photo,
			timezone,
			secondarydata,
			address,
			dob
		) VALUES 
		(
			?,
			?,
			?,
			?,
			?,
			?,
			?,
			?,
			?,
			?,
			?,
			?,
			?,
			?,
			?,
			?,
			?
		)", 
			contact.id,
			contact.isfav,
			contact.firstName,
			contact.lastName,
			contact.gender,
			contact.company,
			contact.role,
			contact.email,
			contact.phone,
			contact.notes,
			contact.latitude,
			contact.longitude,
			JSON.stringify(contact.photo),
			contact.timezone,
			contact.secondarydata,
			JSON.stringify(contact.address),
			contact.dob
		);
};

exports.deleteContact = function(id) {
	db.execute("DELETE FROM contact WHERE id=?", id);
};

exports.updateContact = function(contact) {
	db.execute("UPDATE contact SET firstName=?, lastName=?, gender=?, company=?, role=?, email=?, phone=?, notes=?, latitude=?, longitude=?, photo=?, timezone=?, secondarydata=?, address=?, dob=? WHERE id=?", 
			contact.firstName,
			contact.lastName,
			contact.gender,
			contact.company,
			contact.role,
			contact.email,
			contact.phone,
			contact.notes,
			contact.latitude,
			contact.longitude,
			JSON.stringify(contact.photo),
			contact.timezone,
			contact.secondarydata,
			JSON.stringify(contact.address),
			contact.dob,
			contact.id);
};

exports.contactFav = function(isFav, id) {
	db.execute("UPDATE contact SET isfav = ? WHERE id=?", isFav, id);
};

exports.getContacts = function(){
	var contacts = [];
	var rows = db.execute("SELECT * FROM contact");
	while (rows.isValidRow()){
		contacts.push({
			id : rows.fieldByName("id"),
			isfav : parseInt(rows.fieldByName("isfav")),
			firstName : rows.fieldByName("firstName"),
			lastName : rows.fieldByName("lastName"),
			gender : rows.fieldByName("gender"),
			company : rows.fieldByName("company"),
			role : rows.fieldByName("role"),
			email : rows.fieldByName("email"),
			phone : rows.fieldByName("phone"),
			notes : rows.fieldByName("notes"),
			latitude : rows.fieldByName("latitude"),
			longitude : rows.fieldByName("longitude"),
			photo : JSON.parse(rows.fieldByName("photo")),
			timezone : rows.fieldByName("timezone"),
			secondarydata : rows.fieldByName("secondarydata"),
			address : JSON.parse(rows.fieldByName("address")),
			dob : rows.fieldByName("dob")
		});
	  rows.next();
	}

	return contacts;
};

exports.getContactsOrdered = function(orderBy, dir){
	var contacts = [];
	var rows = db.execute("SELECT * FROM contact ORDER BY " + orderBy + ' ' + dir);
	while (rows.isValidRow()){
		contacts.push({
			id : rows.fieldByName("id"),
			isfav : parseInt(rows.fieldByName("isfav")),
			firstName : rows.fieldByName("firstName"),
			lastName : rows.fieldByName("lastName"),
			gender : rows.fieldByName("gender"),
			company : rows.fieldByName("company"),
			role : rows.fieldByName("role"),
			email : rows.fieldByName("email"),
			phone : rows.fieldByName("phone"),
			notes : rows.fieldByName("notes"),
			latitude : rows.fieldByName("latitude"),
			longitude : rows.fieldByName("longitude"),
			photo : JSON.parse(rows.fieldByName("photo")),
			timezone : rows.fieldByName("timezone"),
			secondarydata : rows.fieldByName("secondarydata"),
			address : JSON.parse(rows.fieldByName("address")),
			dob : rows.fieldByName("dob")
		});
	  rows.next();
	}

	return contacts;
};

exports.insertGroup = function(group){
	db.execute("INSERT INTO contact_grp (title, description, photo, contact_id, isfav) VALUES (?, ?, ?, ?, ?)",
		group.title,
		group.description,
		JSON.stringify(group.photo),
		JSON.stringify(group.contact_id),
		group.isfav
	);
};

exports.updateGroup = function(group) {
	db.execute("UPDATE contact_grp SET title=?, description=?, photo=?, contact_id=? WHERE id=?", 
			group.title,
			group.description,
			JSON.stringify(group.photo),
			JSON.stringify(group.contact_id),
			group.id
	);
};

exports.groupFav = function(isFav, id) {
	db.execute("UPDATE contact_grp SET isfav = ? WHERE id=?", isFav, id);
};

exports.deleteGroup = function(id) {
	db.execute("DELETE FROM contact_grp WHERE id=?", id);
};

exports.getGroups = function(){
	db.execute('CREATE TABLE IF NOT EXISTS contact_grp (id INTEGER PRIMARY KEY AUTOINCREMENT, title TEXT null, description TEXT null, photo TEXT null,contact_id TEXT null, isfav INTEGER null)');
	
	var groups = [];
	var rows = db.execute("SELECT * FROM contact_grp");
	while (rows.isValidRow()){
		groups.push({
			id : rows.fieldByName("id"),
			title : rows.fieldByName("title"),
			description : rows.fieldByName("description"),
			photo: JSON.parse(rows.fieldByName("photo")),
			contact_id: JSON.parse(rows.fieldByName("contact_id")),
			isfav: rows.fieldByName("isfav")
		});
	  	rows.next();
	}

	return groups;
};

exports.getGroupFavs = function(){
	
	var groups = [];
	var rows = db.execute("SELECT * FROM contact_grp WHERE isfav > 0");
	while (rows.isValidRow()){
		groups.push({
			id : rows.fieldByName("id"),
			title : rows.fieldByName("title"),
			description : rows.fieldByName("description"),
			photo: rows.fieldByName("photo"),
			contact_id: rows.fieldByName("contact_id"),
			isfav: rows.fieldByName("isfav")
		});
	  	rows.next();
	}

	return groups;
};
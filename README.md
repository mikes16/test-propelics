#TiAddressBook

##Summary
Thank you for taking the time of going through this exercise. We always enjoy getting to know all applicants better by reading at their code practices and skills. In this exercise we try to take you to a real life scenario where not only you have to code against the clock, but also deal with other developer’s code and client requests on the fly.

Make sure you read through all functionality and issue lists and that you understand them just fine.

We expect you to deliver us two things in the next 2 days:

- A link to a repo (Github, Bitbucket) with the modified code base.
- An assessment of what did you notice on this code that made you things difficult to read or implement and how did you make it better. Include any non-documented bugs/issues you might have found.

Ready? Let’s go.

##Roleplay
A very important client has recently handed off a project to us for finishing it and pushing it to UAT (User Acceptance Testing) in the next two days. This project was initially coded by some marketing agency where they have nearly no code standards, low JS and Ti skills and they ran out of time. It’s an Address Book app they’ll use in their shared facilities with other companies and they want all of their employees to use it. 

Our client’s QA team already has identified a long list of bugs in the current implementation and there’s also a list of missing features they want us to finish to implement. 

We must make sure the app is stable enough and to resolve the highest amount of high and medium priority bugs as well as to implement what we have committed to finish so the client can proceed to send this app to their UAT team. However, we know the level of poor quality of the current code and we must be prepared to deliver an assessment to the client in regards to what we noticed the bad practices the last development team made while implementing their code and how this made us difficult to progress and to complete our goal as well as a list of any non documented issues/bugs we found (if any).

##Rules
- Make sure the app runs using the latest tools
	- Titanium SDK
	- Alloy
	- Android SDK
	- iOS SDK
- The Issues list includes a brief description of the problem and the platforms affected.
- The Features list includes a brief description of what the feature is suposed to do and a set of rules to follow in order to mark it as compete.
- All Issues and Features include a priority (low, medium, high).
- Make sure you commit all your possible changes in GitHub/BitBucket by the due date.
- If you find some non documented issues, add them in a separate list and feel free to solve them.

##Known Issues

###Contacts List
1. Toolbar icons (favorites button, grid button, list button) won’t display on any view (list, grid view). **[Android]** **[Low]**
1. Contact's images are not displaying on any view (list, grid view). **[Android/iOS]** **[Low]**
1. Grid view should display 3 contact tiles that are distributed across screen’s width leaving 10px of left and right margins. **[Android/iOS]** **[Low]**
1. Grid view should scroll vertically only. **[iOS]** **[Low]**
1. Grid view should scroll vertically. Currently it doesn’t scroll at all. **[Android]** **[Low]**
1. List view should show contact’s image, name (first and last) and phone(s) only. **[Android/iOS]** **[Medium]**
1. User should select one contact as "me". **[Android/iOS]** **[High]**
1. Contact selected as "me" should show on top of the list and be hidden form the contacts list. **[Android/iOS]** **[Medium]**
1. If there is no contact selected as "me" that part should be hidden. **[Android/iOS]** **[Medium]**
1. Contact info should not be scrollable inside. **[iOS]** **[Low]**
1. Selecting a contact should open a detail window, with the option to edit all the data for that contact. **[Android/iOS]** **[High]**
1. Swipe to delete a contact is implemented, but that deleted contact re-appears after closing and opening the app. **[iOS]** **[High]**
1. Implement a way to delete a contact **[Android]** **[High]**
1. After adding a new contact and trying to go to into grid view, a runtime exception occurs **[iOS, Android]** **[Medium]**

###New Contact
1. The textfield loses focus after typing 1 letter. **[iOS]** **[Medium]**
1. The textfield loses focus right after gaining it. **[Android]** **[Medium]**
1. Email field should show an email keyboard. **[Android/iOS]** **[Low]**
1. Phone field should show a numberic keyboard. **[Android/iOS]** **[Low]**
1. Gender field should show a list instead of being free to write. **[Android/iOS]** **[Low]**
1. Location opens the photo dialog. **[Android/iOS]** **[Low]**
1. The given location is not getting saved. **[Android/iOS]** **[Medium]**
1. The selected photo is not getting saved. **[Android/iOS]** **[Medium]**
1. The given address is not getting saved. **[Android/iOS]** **[Medium]**
1. The given Date of Birth is not getting saved. **[Android/iOS]** **[Medium]**
1. The selected photo can't be deleted. **[Android/iOS]** **[Low]**
1. In Address screen, the textfield loses focus after typing 1 letter. **[iOS]** **[Low]**
1. In Address screen, the textfield loses focus right after gaining it. **[Android]** **[Low]**
1. After saving an address, the info should be shown with the format: `street, city, state, zip`. If some of the fields is left empty, it should be ignored from the format. **[Android/iOS]** **[Medium]**. Examples:
	- `Street 123, Los Angeles, CA, 12345`
	- `Street 123, CA, 12345`
	- `Street 123, Los Angeles, 12345`
	- `Street 123, 12345`
	- `CA, 12345`
1. Date of Birth picker only shows once. **[Android/iOS]** **[Low]**
1. The user should be saved only if the first name or last name has been filled. **[Android/iOS]** **[Low]**
1. When adding a new contact, a run time error is displayed when trying to add the location field. **[Android]** **[Medium]**
1. When adding a new contact, it’s not possible to drag and drop the pin in the map. **[iOS]** **[Medium]**
1. When adding a new contact, the pin in the map view should show the contact’s name in the pin’s bubble. **[iOS, Android]** **[Low]**
1. Pressing Save in new contact and Address windows doesn't close the window **[Android]** **[Low]**

##Feature requests

###Groups List [High]
Groups are a list of contacts. A contact can be inside more than 1 group

1. Implement a way to add a new Group **[Android/iOS]**
1. Groups should include this data, besides the list of contacts: **[Android/iOS]**
	- Title
	- Notes
	- Photo
1. All groups should be shown in the 2nd tab of the screen (Groups) **[Android/iOS]**
1. Implement a way to edit a group **[Android/iOS]**
1. Implement a way to delete a group **[Android/iOS]**

###Favorites [Medium]
Users and groups can be selected as favorites by the user to have a faster access to them

1. Implement a way to mark/unmark contacts and groups as favorites.
1. Implement a "favorites" view in each screen to only show the contacts or groups marked as favorites
1. Implement functionality so when favorites button in the toolbar is pressed, the app filters out all the non favorite contacts and display the favorites **ONLY**. When button gets pressed again, all the non favorites will be displayed again in the list (this function should get built in both List and Grid views).


###Sorting [High]
Implement some different ways of sorting in the contacts list

1. By First Name
1. By Last Name
1. By Age
1. By distance from the phone (comparing phone's location vs contact location)
1. All sortings should be ascending and descending

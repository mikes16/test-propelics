var args = arguments[0] || {};

var contacts = Alloy.Globals.db.getContacts();
var newSel = args.contacts;

for (var i=0;i<contacts.length;i++) {
	
	var isSelected = newSel.indexOf(contacts[i].id);
	
	var contactsRow = Ti.UI.createTableViewRow({
		height:Ti.UI.SIZE,
		width:Ti.UI.SIZE,
		backgroundSelectedColor: 'transparent',
		contact: contacts[i],
		id: contacts[i].id
	});
	
	if(OS_IOS){
		contactsRow.selectedBackgroundColor = 'transparent';
	}
	
	var container_viw = Ti.UI.createView({
		top: 10,
		height : 50,
		bottom:10,
		left:10,
		right :10
	});
	contactsRow.add(container_viw);	
	
	var thumbnail_img = Ti.UI.createImageView({
		height : "40dp",
		top: 0,
		left : 0,
		width : "40dp",
		image: contacts[i].photo.thumbnail,
		borderRadius: 20
	});
	container_viw.add(thumbnail_img);	
			
	var name_lbl = Ti.UI.createLabel({
		height:Ti.UI.SIZE,
		width:Ti.UI.SIZE,
		left: "45dp",
		color: '#000000',
		font: { fontSize : 15 },
		text: contacts[i].firstName + ' ' + contacts[i].lastName
	});
	container_viw.add(name_lbl);
	
	var add_btn = Ti.UI.createButton({
		id: 'agregar',
		width: "30dp",
		height: "30dp",
		title: (isSelected > -1) ? "-" : "+",
		right: 5,
		borderRadius: 15,
		backgroundColor: (isSelected > -1) ? "#E84021" : "#2087FF",
		borderColor: '#BFBFBF',
		color: '#000'
	});
	container_viw.add(add_btn);
	
			
	contactsRow.addEventListener('click', function(evt){
		switch(evt.source.id){
			case 'agregar':
				// $.bookContacts.deleteRow(evt.row);
				
				if(evt.source.title == "+"){
					evt.source.title = "-";
					evt.source.backgroundColor = "#E84021";
					
					newSel.push(evt.rowData.contact.id);
				}else{
					
					evt.source.title = "+";
					evt.source.backgroundColor = "#2087FF";
					
					var index = newSel.indexOf(evt.rowData.contact.id);
					if (index > -1) {
					    newSel.splice(index, 1);
					}
				}
				
				break;
			default : break;
		}
	});
	
	$.bookContacts.appendRow(contactsRow);	
};

function done(){
	args.callback({
		newSel: newSel
	});
	Alloy.Globals.group_tab.close($.window);
	if (OS_ANDROID) {
		$.window.close();
	}
}

// $.bookContacts
var args = arguments[0] || {};

var large, medium, thumbnail, isUpdate;
var contactSelected = [];
$.contactsCount.value = "Seleccionados: " + contactSelected.length;

if(args.group){
	isUpdate = true;
	contactSelected = args.group.contact_id;
	
	largeImg = args.group.photo.large;
	medium = args.group.photo.medium;
	thumbnail = args.group.photo.thumbnail;
	
	$.title.value = args.group.title;
	$.notes.value = args.group.description;
	$.photoimg.image = args.group.photo.thumbnail;
	$.contactsCount.value = "Seleccionados: " + args.group.contact_id.length;	
}

function save () {
	if($.title.value != ""){
		
		if(large && medium && thumbnail){
			var largeImg = Ti.Filesystem.getFile(Ti.Filesystem.applicationDataDirectory,'large_'+$.title.value+'.jpg');
			largeImg.write(large);
			
			var mediumImg = Ti.Filesystem.getFile(Ti.Filesystem.applicationDataDirectory,'medium_'+$.title.value+'.jpg');
			mediumImg.write(medium);
			
			var thumbnailImg = Ti.Filesystem.getFile(Ti.Filesystem.applicationDataDirectory,'thumbnail_'+$.title.value+'.jpg');
			thumbnailImg.write(thumbnail);	
		}
		
		if(isUpdate){
			Alloy.Globals.db.updateGroup({
				id: args.group.id,
				title: $.title.value,
				description: $.notes.value,
				photo : {
					large: (large) ? largeImg.nativePath : '',
					medium: (large) ? mediumImg.nativePath : '',
					thumbnail: (large) ? thumbnailImg.nativePath : ''
				},
				contact_id: contactSelected
			});
		}else{
			Alloy.Globals.db.insertGroup({
				title: $.title.value,
				description: $.notes.value,
				photo : {
					large: (large) ? largeImg.nativePath : '',
					medium: (large) ? mediumImg.nativePath : '',
					thumbnail: (large) ? thumbnailImg.nativePath : ''
				},
				contact_id: contactSelected,
				isfav: 0
			});
		}
		
		Ti.App.fireEvent('refreshGroups');
		Alloy.Globals.group_tab.close($.window);
		
		if (OS_ANDROID) {
			$.window.close();
		}
	}else{
		alert("You need to set title to continue");
	}
	
}

function showPhoto (e) {
	if(e.index==0){
		Ti.Media.showCamera({
			success:function(e2){
				large = e2.media.imageAsResized(512, 512);
				medium = e2.media.imageAsResized(150, 150);
				thumbnail = e2.media.imageAsResized(80, 80);
				$.photoimg.image=e2.media;
			}
		});
	}else if(e.index==1){
		Ti.Media.openPhotoGallery({
			success:function(e2){
				large = e2.media.imageAsResized(512, 512);
				medium = e2.media.imageAsResized(150, 150);
				thumbnail = e2.media.imageAsResized(80, 80);
				$.photoimg.image=e2.media;
			}
		});
	}
}

function removeImage(){
	var dialog = Ti.UI.createAlertDialog({
	    cancel: 1,
	    buttonNames: ['Confirm', 'Cancel'],
	    message: 'Would you like to remove the image?',
	    title: 'Remove'
	});
	 
	dialog.addEventListener('click', function(e){
	    if (e.index === e.source.cancel){
	      	Ti.API.info('The cancel button was clicked');
	    }else{
	    	$.photoimg.image = "";	
	    	large = "";
			medium = "";
			thumbnail = "";
	    }
	});
	dialog.show();
}

//OPEN THE OPTIONS TO OPEN THE CAMERA
function openPhotoOptions(){
	$.photoOptions.show();
}

//OPEN A NEW WINDOW WITH THE CONTACTS
function openContacts(){
	contacts=Alloy.createController("contacts", {
		contacts: contactSelected,
		callback:function(evt){
			contactSelected = evt.newSel;
			$.contactsCount.value = "Seleccionados: " + contactSelected.length;
		}
	});
	Alloy.Globals.group_tab.open(contacts.window);
}
